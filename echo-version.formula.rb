class EchoVersion < Formula
  desc "Echo some hardcoded line"
  homepage "https://gitlab.com/antik9/echo-version"
  url "https://gitlab.com/antik9/echo-version/-/raw/<VERSION>/echo-version"
  version "<VERSION>"
  license "MIT"

  def install
    bin.install "echo-version"
  end

  test do
    system bin/"echo-version"
  end
end
