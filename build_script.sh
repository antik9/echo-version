#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

export ECHO_VERSION_DIR="$PWD"
export BREW_TAP_BRANCH_NAME="feature/${CI_COMMIT_TAG//./-}"

function clone_git_repo() {
    git clone https://gitlab.com/antik9/homebrew-pack "$BUILD_DIR"
    cd "$BUILD_DIR"

    git config user.email "$GITLAB_USER_EMAIL"
    git config user.name "$GITLAB_USER_NAME"
    git remote set-url origin "https://${BREW_TAP_USERNAME}:${BREW_TAP_PASSWORD}@gitlab.com/antik9/homebrew-pack"
}

function prepare_new_branch() {
    git checkout -b "$BREW_TAP_BRANCH_NAME"
    mkdir -p Formula
}

function commit_changes() {
    sed -e "s/<VERSION>/$CI_COMMIT_TAG/g" "$ECHO_VERSION_DIR/echo-version.formula.rb" > Formula/echo-version.rb
    git add Formula/echo-version.rb
    git commit -am "$COMMIT_MESSAGE"
    git push origin "$BREW_TAP_BRANCH_NAME" -o merge_request.create
}

function main() {
    clone_git_repo
    prepare_new_branch
    commit_changes
}

# End sourced section
return 2> /dev/null

main
